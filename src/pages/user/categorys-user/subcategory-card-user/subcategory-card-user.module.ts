import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubCategorysCardUserPage } from './subcategory-card-user';
 
@NgModule({
  declarations: [
    SubCategorysCardUserPage,
  ],
  imports: [
    IonicPageModule.forChild(SubCategorysCardUserPage),
  ],
  exports: [
    SubCategorysCardUserPage
  ]
})
export class SubCategorysCardUserPageModule {}