import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CronometroUserPage } from './cronometro-user';
 
@NgModule({
  declarations: [
    CronometroUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CronometroUserPage),
  ],
  exports: [
    CronometroUserPage
  ]
})
export class CronometroUserPageModule {}