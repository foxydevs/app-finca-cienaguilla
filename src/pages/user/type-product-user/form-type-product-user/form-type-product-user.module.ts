import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormTypeProductUserPage } from './form-type-product-user';
import { PipeModule } from '../../../../pipes/pipes.module';
import { PresentationService } from '../../../../app/service/presentation.service';
 
@NgModule({
  declarations: [
    FormTypeProductUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormTypeProductUserPage),
    PipeModule
  ],
  exports: [
    FormTypeProductUserPage
  ],
  providers: [
    PresentationService
  ]
})
export class FormTypeProductUserPageModule {}