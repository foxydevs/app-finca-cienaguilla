import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitesUserPage } from './cites-user';
import { NgCalendarModule } from 'ionic2-calendar';
 
@NgModule({
  declarations: [
    CitesUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CitesUserPage),
    NgCalendarModule,
  ],
  exports: [
    CitesUserPage
  ]
})
export class CitesUserPageModule {}