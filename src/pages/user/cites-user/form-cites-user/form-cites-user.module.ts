import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormCitesUserPage } from './form-cites-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormCitesUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormCitesUserPage),
    PipeModule
  ],
  exports: [
    FormCitesUserPage
  ]
})
export class FormCitesUserPageModule {}