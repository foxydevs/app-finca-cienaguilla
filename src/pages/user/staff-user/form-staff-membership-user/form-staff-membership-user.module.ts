import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { FormStaffMembershipUserPage } from './form-staff-membership-user';
 
@NgModule({
  declarations: [
    FormStaffMembershipUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormStaffMembershipUserPage),
    PipeModule
  ],
  exports: [
    FormStaffMembershipUserPage
  ]
})
export class FormStaffMembershipUserPageModule {}