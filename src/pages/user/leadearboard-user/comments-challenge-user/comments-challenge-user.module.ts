import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommentsChallengeUserPage } from './comments-challenge-user';
 
@NgModule({
  declarations: [
    CommentsChallengeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CommentsChallengeUserPage),
  ],
  exports: [
    CommentsChallengeUserPage
  ]
})
export class CommentsChallengeUserPageModule {}