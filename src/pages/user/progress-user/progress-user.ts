import { Component } from '@angular/core';
import { NavController, AlertController, IonicPage } from 'ionic-angular';
import { UsersService } from '../../../app/service/users.service';
import { LoadingController } from '../../../../node_modules/ionic-angular/components/loading/loading-controller';
import { ProgressService } from '../../../app/service/progress.service';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'progress-user',
  templateUrl: 'progress-user.html'
})
export class ProgressUserPage {
  //PROPIEDADES
  public idUser:any;
  public search:any;
  public search2:any;
  public users:any[] = [];
  public table:any[] = [];
  selectItem:any = 'progresos';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: UsersService,
    public alertCtrl: AlertController,
    public secondService: ProgressService,
    public modalController: ModalController
  ) {
    this.idUser = localStorage.getItem('currentId')
  }

  //CARGAR USUARIOS
  public getAll(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.mainService.getClients(this.idUser)
    .then(response => {
      this.users = [];
      this.users = response;
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //CARGAR USUARIOS
  public getAllSecond(){
    let load = this.loading.create({
      content: "Cargando..."
    });
    load.present();
    this.secondService.getAllUser(this.idUser)
    .then(response => {
      this.table = [];
      this.table = response;
      this.table.reverse();
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList button").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //BUSCAR
  public searchTable2() {
    var value = this.search2.toLowerCase();
    $("#myList ion-item").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //OPEN MODAL
  public openModalCrate(idClient:any) {
    let parameter = {
      idClient: idClient
    }
    let chooseModal = this.modalController.create('FormProgressUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        if(this.selectItem == 'progresos') {
          this.getAllSecond();        
        } else {
          this.getAll()
        }
      }
    });
    chooseModal.present();
  }

  //OPEN MODAL
  public openModalUpdate(parameter:any) {
    let chooseModal = this.modalController.create('FormProgressUserPage', { parameter });
    chooseModal.onDidDismiss((data) => {
      if(data!='Close') {
        if(this.selectItem == 'progresos') {
          this.getAllSecond();        
        } else {
          this.getAll()
        }
      }
    });
    chooseModal.present();
  }

  /*//OPEN FORM
  openForm(idClient:any) {
    let parameter = {
      idClient: idClient
    }
    this.navCtrl.push('FormProgressUserPage', { parameter })
  }

  //OPEN FORM
  openFormUpdate(parameter:any) {
    this.navCtrl.push('FormProgressUserPage', { parameter })
  }*/

  //ELIMINAR
  public delete(id:string){
    let confirm = this.alertCtrl.create({
      title: '¿Deseas eliminar el progreso del usuario?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.secondService.delete(id)
            .then(response => {
              this.getAllSecond()
              console.clear();
            }).catch(error => {
              console.clear();
            })
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewWillEnter() {
    if(this.selectItem == 'progresos') {
      this.getAllSecond();        
    } else {
      this.getAll()
    }
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      if(this.selectItem == 'progresos') {
        this.getAllSecond();        
      } else {
        this.getAll()
      }
      refresher.complete();
    }, 2000);
  }
}