import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { PresentationService } from '../../../../app/service/presentation.service';
import { ModalPresentacionUserPage } from './modal-presentacion-user';
 
@NgModule({
  declarations: [
    ModalPresentacionUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalPresentacionUserPage),
    PipeModule
  ],
  exports: [
    ModalPresentacionUserPage
  ],
  providers: [
    PresentationService
  ]
})
export class ModalPresentacionUserPageModule {}