import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsUserPage } from './products-user';
 
@NgModule({
  declarations: [
    ProductsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductsUserPage),
  ],
  exports: [
    ProductsUserPage
  ]
})
export class ProductsUserPageModule {}