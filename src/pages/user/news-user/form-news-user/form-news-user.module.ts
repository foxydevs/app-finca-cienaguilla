import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormNewsUserPage } from './form-news-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormNewsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormNewsUserPage),
    PipeModule
  ],
  exports: [
    FormNewsUserPage
  ]
})
export class FormNewsUserPageModule {}