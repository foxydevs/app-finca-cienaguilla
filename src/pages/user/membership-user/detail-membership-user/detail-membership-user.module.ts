import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailMembershipUserPage } from './detail-membership-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailMembershipUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailMembershipUserPage),
    PipeModule
  ],
  exports: [
    DetailMembershipUserPage
  ]
})
export class DetailMembershipUserPageModule {}