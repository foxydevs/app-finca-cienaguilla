import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategorysDocumentsUserPage } from './categorys-documents-user';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    CategorysDocumentsUserPage,
  ],
  imports: [
    IonicPageModule.forChild(CategorysDocumentsUserPage),
    PipeModule
  ],
  exports: [
    CategorysDocumentsUserPage
  ]
})
export class CategorysDocumentsUserPageModule {}