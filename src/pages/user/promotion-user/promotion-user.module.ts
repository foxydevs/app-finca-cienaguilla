import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromotionUserPage } from './promotion-user';
import { DescuentoService } from '../../../app/service/descuentos.service';
 
@NgModule({
  declarations: [
    PromotionUserPage,
  ],
  imports: [
    IonicPageModule.forChild(PromotionUserPage),
  ],
  exports: [
    PromotionUserPage
  ], providers: [
    DescuentoService
  ]
})
export class PromotionUserPageModule {}