import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnuncioUserPage } from './anuncio-user';
import { SliderService } from '../../../app/service/sliders.service';
 
@NgModule({
  declarations: [
    AnuncioUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AnuncioUserPage),
  ],
  exports: [
    AnuncioUserPage
  ], providers: [
    SliderService
  ]
})
export class BlogUserPageModule {}