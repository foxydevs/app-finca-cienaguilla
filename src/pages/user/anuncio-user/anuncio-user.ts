import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { SliderService } from '../../../app/service/sliders.service';
import { path } from '../../../app/config.module';

@IonicPage()
@Component({
  selector: 'anuncio-user',
  templateUrl: 'anuncio-user.html',
})
export class AnuncioUserPage {
  //PROPIEDADES
  public table:any[] = [];
  public months:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public loading: LoadingController,
    public mainService: SliderService
  ) {
  }

  openForm(parameter?:any) {
    this.navCtrl.push('ModalAnuncioUserPage', { parameter });
  }

  //CARGAR LOS RETOS
  public getAll() {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getAllUser(path.id)
    .then(response => {
      this.table = []
      this.table = response;
      load.dismiss();
    }).catch(error => {
      load.dismiss();
      console.clear
    })
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:any) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth() + 1)] + ', ' + date.getFullYear();
    return created_at;
  }

  ionViewWillEnter() {
    this.getMonths();
    this.getAll();
  }

}
