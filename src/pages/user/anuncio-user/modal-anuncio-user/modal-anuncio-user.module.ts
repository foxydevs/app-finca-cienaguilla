import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { ModalAnuncioUserPage } from './modal-anuncio-user';
import { SliderService } from '../../../../app/service/sliders.service';
 
@NgModule({
  declarations: [
    ModalAnuncioUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAnuncioUserPage),
    PipeModule
  ],
  exports: [
    ModalAnuncioUserPage
  ], providers: [
    SliderService
  ]
})
export class FormBlogUserPageModule {}