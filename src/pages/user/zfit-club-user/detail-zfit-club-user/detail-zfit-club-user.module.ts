import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailZFitClubUserPage } from './detail-zfit-club-user';
import { PipeModule } from '../../../../pipes/pipes.module';
import { SocialSharing } from '../../../../../node_modules/@ionic-native/social-sharing';
 
@NgModule({
  declarations: [
    DetailZFitClubUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailZFitClubUserPage),
    PipeModule
  ],
  exports: [
    DetailZFitClubUserPage
  ],
  providers: [
    SocialSharing
  ]
})
export class DetailZFitClubUserPageModule {}