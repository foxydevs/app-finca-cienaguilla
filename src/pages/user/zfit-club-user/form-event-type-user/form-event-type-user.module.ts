import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormEventTypeUserPage } from './form-event-type-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormEventTypeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormEventTypeUserPage),
    PipeModule
  ],
  exports: [
    FormEventTypeUserPage
  ]
})
export class FormEventTypeUserPageModule {}