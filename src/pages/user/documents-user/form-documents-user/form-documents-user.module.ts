import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormDocumentUserPage } from './form-documents-user';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    FormDocumentUserPage,
  ],
  imports: [
    IonicPageModule.forChild(FormDocumentUserPage),
    PipeModule
  ],
  exports: [
    FormDocumentUserPage
  ]
})
export class FormDocumentUserPageModule {}