import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { YoutubeUserPage } from './youtube-user';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    YoutubeUserPage,
  ],
  imports: [
    IonicPageModule.forChild(YoutubeUserPage),
    PipeModule
  ],
  exports: [
    YoutubeUserPage
  ]
})
export class YoutubeUserPageModule {}