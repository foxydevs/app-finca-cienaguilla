import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { AddressService } from '../../../../app/service/address.service';
import { MyCartQPPClient } from './my-cart-qpp-client';
import { DescuentoService } from '../../../../app/service/descuentos.service';
 
@NgModule({
  declarations: [
    MyCartQPPClient,
  ],
  imports: [
    IonicPageModule.forChild(MyCartQPPClient),
    PipeModule
  ],
  exports: [
    MyCartQPPClient
  ], providers: [
    AddressService,
    DescuentoService
  ]
})
export class MyCartQPPClientModule {}