import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { WorkoutsService } from '../../../../app/service/workouts.service';

@IonicPage()
@Component({
  selector: 'comments-workouts-client',
  templateUrl: 'comments-workouts-client.html'
})
export class CommentsWorkoutsClientPage{
  public comment = {
    comment: '',
    reto : '',
    user: ''
  }
  public parameter:any;
  //public baseId:number = path.id;
  public btnDisabled:boolean;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public mainService: WorkoutsService,
    public viewCtrl: ViewController,
    public loading: LoadingController
  ) {
    this.btnDisabled = false;
    this.parameter = this.navParams.get('parameter');
    this.comment.reto = this.parameter;
    this.comment.user = localStorage.getItem('currentId');
  }

   //Insertar Datos
   insert(){
    //let id = this.comment.product;
    if(this.comment.comment) {
      this.btnDisabled = true;
      let load =  this.loading.create({
        content: "Registrando..."
      });
      load.present();   
      this.mainService.createComment(this.comment)
      .then(response => {
          console.log(response)
        this.navCtrl.pop();
        load.dismiss();
      }).catch(error => {
          console.log(error)
          this.btnDisabled = false;
      })
    } else {
      this.toast.create({
        message: "Ingrese un comentario.",
        duration: 800
      }).present();
    }
  }
}
