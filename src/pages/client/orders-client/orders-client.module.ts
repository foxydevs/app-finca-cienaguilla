import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersClientPage } from './orders-client';
import { PipeModule } from '../../../pipes/pipes.module';
import { DescuentoService } from '../../../app/service/descuentos.service';
 
@NgModule({
  declarations: [
    OrdersClientPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdersClientPage),
    PipeModule
  ],
  exports: [
    OrdersClientPage
  ],
  providers:[
    DescuentoService
  ]
})
export class OrdersClientPageModule {}