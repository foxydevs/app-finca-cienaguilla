import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeTipsClientCommentPage } from './see-tips-client-comment';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    SeeTipsClientCommentPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeTipsClientCommentPage),
    PipeModule
  ],
  exports: [
    SeeTipsClientCommentPage
  ]
})
export class SeeTipsClientCommentPageModule {}