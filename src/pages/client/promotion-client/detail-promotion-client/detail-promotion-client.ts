import { Component } from '@angular/core';
import { NavController, LoadingController, IonicPage} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PromotionService } from '../../../../app/service/promotion.service';

@IonicPage()
@Component({
  selector: 'detail-promotion-client',
  templateUrl: 'detail-promotion-client.html',
})
export class DetailPromotionClientPage {
  //PROPIEDADES
  public parameter:any;
  public data = {
    title: '',
    description: '',
    inicio: '',
    fin: '',
    descuento: ''
  }
  public months:any[] = [];
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public mainService: PromotionService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getMonths();
    this.getSingle(this.parameter);
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      this.data.inicio = this.returnDate(response.inicio);
      this.data.fin = this.returnDate(response.fin);
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //CARGAR MESES
  public getMonths() {
    this.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];
  }

  //CAMBIAR EL FORMATO DE LA FECHA Y HORA//CAMBIAR FORMATO DE FECHA
  public returnDate(fechaJSON:string) {
    var date = new Date(fechaJSON);
    var created_at = date.getDate() + ' ' + this.months[(date.getMonth())] + ', ' + date.getFullYear();
    return created_at;
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getSingle(this.parameter);
      refresher.complete();
    }, 2000);
  }

}
