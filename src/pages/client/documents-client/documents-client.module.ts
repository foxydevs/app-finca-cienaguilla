import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DocumentsClientPage } from './documents-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DocumentsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DocumentsClientPage),
    PipeModule
  ],
  exports: [
    DocumentsClientPage
  ]
})
export class DocumentsClientPageModule {}