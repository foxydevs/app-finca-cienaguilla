import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { ModalAddressUserPage } from './modal-address-client';
import { AddressService } from '../../../../app/service/address.service';
 
@NgModule({
  declarations: [
    ModalAddressUserPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddressUserPage),
    PipeModule
  ],
  exports: [
    ModalAddressUserPage
  ],
  providers: [
    AddressService
  ]
})
export class ModalAddressUserPageModule {}