import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailMotivationUserPage } from './detail-motivations-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailMotivationUserPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailMotivationUserPage),
    PipeModule
  ],
  exports: [
    DetailMotivationUserPage
  ]
})
export class DetailMotivationUserPageModule {}