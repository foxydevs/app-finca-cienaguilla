import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailActivePauseClientPage } from './detail-active-pause-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailActivePauseClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailActivePauseClientPage),
    PipeModule
  ],
  exports: [
    DetailActivePauseClientPage
  ]
})
export class DetailActivePauseClientPageModule {}