import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailZFitClubClientPage } from './detail-zfit-club-client';
import { PipeModule } from '../../../../pipes/pipes.module';
import { SocialSharing } from '../../../../../node_modules/@ionic-native/social-sharing';
 
@NgModule({
  declarations: [
    DetailZFitClubClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailZFitClubClientPage),
    PipeModule
  ],
  exports: [
    DetailZFitClubClientPage
  ],
  providers: [
    SocialSharing
  ]
})
export class DetailZFitClubClientPageModule {}