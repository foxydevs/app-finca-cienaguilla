import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ZFitClubClientPage } from './zfit-club-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    ZFitClubClientPage,
  ],
  imports: [
    IonicPageModule.forChild(ZFitClubClientPage),
    PipeModule
  ],
  exports: [
    ZFitClubClientPage
  ]
})
export class ZFitClubClientPageModule {}