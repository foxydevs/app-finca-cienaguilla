import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailRunningClubClientPage } from './detail-running-club-client';
import { PipeModule } from '../../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    DetailRunningClubClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailRunningClubClientPage),
    PipeModule
  ],
  exports: [
    DetailRunningClubClientPage
  ]
})
export class DetailRunningClubClientPageModule {}