import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormCitesClientPage } from './form-cites-client';
import { PipeModule } from '../../../../pipes/pipes.module';
import { NgCalendarModule } from 'ionic2-calendar';
 
@NgModule({
  declarations: [
    FormCitesClientPage,
  ],
  imports: [
    IonicPageModule.forChild(FormCitesClientPage),
    PipeModule,
    NgCalendarModule,
  ],
  exports: [
    FormCitesClientPage
  ]
})
export class FormCitesClientPageModule {}