import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeeEventsClientPage } from './see-events-client';
import { PipeModule } from '../../../../pipes/pipes.module';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    SeeEventsClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeEventsClientPage),
    PipeModule
  ],
  exports: [
    SeeEventsClientPage
  ],
  providers: [
    SocialSharing
  ]
})
export class SeeEventsClientPageModule {}