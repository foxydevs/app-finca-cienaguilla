import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { SeeYoutubeClientPage } from './see-youtube-client';
 
@NgModule({
  declarations: [
    SeeYoutubeClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SeeYoutubeClientPage),
    PipeModule
  ],
  exports: [
    SeeYoutubeClientPage
  ]
})
export class SeeYoutubeClientPageModule {}