import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubCategorysCardClientPage } from './subcategorys-card-client';
 
@NgModule({
  declarations: [
    SubCategorysCardClientPage,
  ],
  imports: [
    IonicPageModule.forChild(SubCategorysCardClientPage),
  ],
  exports: [
    SubCategorysCardClientPage
  ]
})
export class SubCategorysCardClientPageModule {}