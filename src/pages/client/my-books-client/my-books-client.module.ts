import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../pipes/pipes.module';
import { MyBooksClientPage } from './my-books-client';
 
@NgModule({
  declarations: [
    MyBooksClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MyBooksClientPage),
    PipeModule
  ],
  exports: [
    MyBooksClientPage
  ]
})
export class MyBooksClientPageModule {}