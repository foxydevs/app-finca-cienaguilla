import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { InAppBrowser } from '../../../../node_modules/@ionic-native/in-app-browser';
import { Platform } from 'ionic-angular/platform/platform';
import { IonicPage } from '../../../../node_modules/ionic-angular/navigation/ionic-page';
import { OrdersService } from '../../../app/service/orders.service';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'my-books-client',
  templateUrl: 'my-books-client.html'
})
export class MyBooksClientPage {
  //PROPIEDADES
  public orders:any[] = [];
  public parameter:any;
  public search:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  idClient = localStorage.getItem('currentId');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public mainService: OrdersService,
    public loading: LoadingController,
    public alertCtrl:AlertController,
    public navParams: NavParams,
    public iab: InAppBrowser,
    public platform: Platform,
    public toast: ToastController
  ) {
  }

  //CARGAR ORDENES
  public getAll(id:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getForClients(id)
    .then(response => {
      this.orders = []
      this.orders = response;
      this.orders.reverse();
      load.dismiss();
    }).catch(error => {
      console.clear
    })
  }

  ionViewWillEnter() {
    this.getAll(this.idClient);
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any, title:any) {
    //this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    if (this.platform.is('android')) {
      this.iab.create('http://docs.google.com/gview?url=' + urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#003D6E');      
    } else {
      this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet');
    }
  }

  //DOCUMENTO NO DISPONIBLE
  documentEmpty() {
    this.message('El documento no se encuentra disponible.');
  }

  //BUSCAR
  public searchTable() {
    var value = this.search.toLowerCase();
    $("#myList ion-card").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //REFRESCAR
  doRefresh(refresher) {
    setTimeout(() => {
      this.getAll(this.idClient);      
      refresher.complete();
    }, 2000);
  }

}
