import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MotivationClientPage } from './motivation-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    MotivationClientPage,
  ],
  imports: [
    IonicPageModule.forChild(MotivationClientPage),
    PipeModule
  ],
  exports: [
    MotivationClientPage
  ]
})
export class MotivationClientPageModule {}