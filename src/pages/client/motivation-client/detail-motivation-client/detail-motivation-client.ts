import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, IonicPage} from 'ionic-angular';
import { MotivationService } from '../../../../app/service/motivation.service';
import { path } from '../../../../app/config.module';
import { LoadingController } from '../../../../../node_modules/ionic-angular/components/loading/loading-controller';

@IonicPage()
@Component({
  selector: 'detail-motivation-client',
  templateUrl: 'detail-motivation-client.html',
})
export class DetailMotivationsUserPage {
  //PROPIEDADES
  public parameter:any;
  selectItem:any = 'actualizar';
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  public data = {
    title: '',
    description: '',
    user: path.id,
    picture: '',
    id: ''
  }

  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toast: ToastController,
    public loading: LoadingController,
    public mainService: MotivationService
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }
}
