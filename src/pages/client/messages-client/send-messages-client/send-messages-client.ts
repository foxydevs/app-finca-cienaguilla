import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, IonicPage } from 'ionic-angular';
import { MessagesService } from '../../../../app/service/messages.service';
import { UsersService } from '../../../../app/service/users.service';

import { path } from "./../../../../app/config.module";
import { ViewController } from 'ionic-angular/navigation/view-controller';
//import { ActionSheetController } from 'ionic-angular/components/action-sheet/action-sheet-controller';

//JQUERY
declare var $:any;

@IonicPage()
@Component({
  selector: 'send-messages-client',
  templateUrl: 'send-messages-client.html'
})
export class SendMessagesClientPage{
  public users:any[] = [];
  public baseId:number = path.id;
  public btnDisabled:boolean;
  public btnEnabled:boolean;
  public parameter:any;
  public basePath:string = path.path;
  public message = {
    subject: '',
    message : '',
    user_send: '',
    id: '',
    picture: 'https://images.vexels.com/media/users/3/136394/isolated/preview/83b45fcfc188a8f4a9daef936855b019-icono-de-mensaje-by-vexels.png',
    user_receipt: '',
    tipo: 0,
  }
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    public messagesService: MessagesService,
    public usersService: UsersService,
    public loading: LoadingController,
    public viewCtrl: ViewController,
    //public actionSheetCtrl: ActionSheetController,
  ) {
    this.parameter = this.navParams.get('parameter');
    this.btnEnabled = true;
    this.message.user_send = localStorage.getItem('currentId');
    this.loadAllUsers();
    console.log(this.parameter)
    if(this.parameter) {
      this.message.user_receipt = this.parameter;
    } else {
      this.message.user_receipt = this.baseId.toString();
    }
    if(localStorage.getItem('currentStaff')) {
      this.message.tipo = 1;
    }
  }

   //Insertar Datos
   public sendMessage(){
    this.message.picture = $('img[alt="Avatar"]').attr('src');
    console.log(this.message.picture)
    if(this.message.message) {
      this.btnDisabled = true;
      if(this.message.picture == 'https://images.vexels.com/media/users/3/136394/isolated/preview/83b45fcfc188a8f4a9daef936855b019-icono-de-mensaje-by-vexels.png') {
        this.message.picture = '';
        this.messagesService.create(this.message)
        .then(response => {
          this.loading.create({
            content: "Enviando Mensaje...",
            duration: 2000
          }).present();
          console.log(response)
          //this.message.id = response.id;
          //this.viewCtrl.dismiss();
          this.navCtrl.pop();
          console.clear
        }).catch(error => {
          this.btnDisabled = false;
          console.clear
        });
      } else {
        this.messagesService.create(this.message)
        .then(response => {
          this.loading.create({
            content: "Enviando Mensaje...",
            duration: 2000
          }).present();
          console.log(response)
          //this.message.id = response.id;
          //this.viewCtrl.dismiss();
          this.navCtrl.pop();
          console.clear
        }).catch(error => {
          this.btnDisabled = false;
          console.clear
        });
      }
    } else {      
      this.messages('El mensaje es requerido.');
    }    
  }

  //Cargar los productos
  public loadAllUsers(){
    this.usersService.getAll()
    .then(response => {
      this.users = response;
    }).catch(error => {
      console.clear
    })
  }

  public returnMessages() {
    this.navCtrl.push('MessagesClientPage');
  }

  //IMAGEN DE CATEGORIA
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    console.log(archivos[0])
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg") {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif')
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'messages'
          },
          function(respuesta) {
            $('#imgAvatar').attr("src", respuesta.url)
            $("#"+id).val('')
          }
        );
      } else {
        this.messages('La imagen es demasiado grande.')
      }
    } else {
      this.messages('El tipo de imagen no es válido.')
    }
  }

  //MENSAJES
  public messages(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

}
