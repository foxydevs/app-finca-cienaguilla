import { Component, Renderer } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InformationService } from '../../../../app/service/information.service';
import { UsersService } from '../../../../app/service/users.service';
import { ScheduleService } from '../../../../app/service/schedule.service';
import { path } from '../../../../app/config.module';
import { ViewController } from '../../../../../node_modules/ionic-angular/navigation/view-controller';

//JQUERY
declare var google;

@IonicPage()
@Component({
  selector: 'ubication-modal-client',
  templateUrl: 'ubication-modal-client.html'
})
export class UbicationModalClientPage {
  //PROPIEDADES
  public data:any = [];
  public map: any;
  public idUser:any = path.id;
  public last_latitud:any;
  public last_longitud:any;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  constructor(
    public navCtrl: NavController,
    public userService: UsersService,
    public iab: InAppBrowser,
    public mainService: InformationService,
    public secondService: ScheduleService,
    public viewCtrl: ViewController,
    public renderer: Renderer,
  ) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
    this.userService.getSingle(this.idUser)
    .then(response => {
    this.last_latitud = response.last_latitud;
    this.last_longitud = response.last_longitud;
    this.data = response;
      this.loadMapUpdate(this.last_latitud, this.last_longitud);
    }).catch(error => {
        console.clear()
    })
  }

  //CERRAR MODAL
  public closeModal() {
    this.viewCtrl.dismiss('Close');
  }

  //CARGAR MAPA ACTUALIZAR
  public loadMapUpdate(lat:any, lon:any) {
  let latitude = lat;
  let longitude = lon;
  this.last_latitud = latitude.toString();
  this.last_longitud = longitude.toString();
    let mapEle: HTMLElement = document.getElementById('map');
    let myLatLng = new google.maps.LatLng({lat: latitude, lng: longitude});
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 17
    });

    var marker;
    marker = new google.maps.Marker({
      map: this.map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: myLatLng
    });

    google.maps.event.addListener(marker, 'dragend', (evt) => {
      this.last_latitud = evt.latLng.lat();
      this.last_longitud = evt.latLng.lng();
    });
  }

  ionViewWillEnter() {
  }

}