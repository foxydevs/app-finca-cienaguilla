import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StaffClientPage } from './staff-client';
import { PipeModule } from '../../../pipes/pipes.module';
 
@NgModule({
  declarations: [
    StaffClientPage,
  ],
  imports: [
    IonicPageModule.forChild(StaffClientPage),
    PipeModule
  ],
  exports: [
    StaffClientPage
  ]
})
export class StaffClientPageModule {}