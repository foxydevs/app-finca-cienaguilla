import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../../pipes/pipes.module';
import { DetailTipsBibliaClientPage } from './detail-tips-biblia-client';
 
@NgModule({
  declarations: [
    DetailTipsBibliaClientPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailTipsBibliaClientPage),
    PipeModule
  ],
  exports: [
    DetailTipsBibliaClientPage
  ]
})
export class DetailTipsBibliaClientPageModule {}