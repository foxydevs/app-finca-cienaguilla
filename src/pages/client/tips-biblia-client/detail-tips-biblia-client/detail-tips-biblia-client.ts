import { Component } from '@angular/core';
import { NavController, LoadingController} from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Clipboard } from '../../../../../node_modules/@ionic-native/clipboard';
import { path } from '../../../../app/config.module';
import { IonicPage } from '../../../../../node_modules/ionic-angular/navigation/ionic-page';
import { TipsBibliaService } from '../../../../app/service/tips-biblia.service';

@IonicPage()
@Component({
  selector: 'detail-tips-biblia-client',
  templateUrl: 'detail-tips-biblia-client.html',
})
export class DetailTipsBibliaClientPage {
  //PROPIEDADES
  public parameter:any;
  public users:any[] = [];
  public comments:any[] = [];
  public data:any[] = [];
  public baseUserId = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');
  
  //CONSTRUCTOR
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loading: LoadingController,
    public toast: ToastController,
    public iab: InAppBrowser,
    public mainService: TipsBibliaService,
    public clipboard: Clipboard
  ) {
    this.parameter = this.navParams.get('parameter');
    this.getSingle(this.parameter);
  }

  //OBTENER DATOS
  getSingle(parameter:any) {
    let load = this.loading.create({
      content: 'Cargando...'
    });
    load.present();
    this.mainService.getSingle(parameter)
    .then(response => {
      this.data = response;
      load.dismiss();
    }).catch(error => {
      console.log(error)
    });
  }

  //Devolver el Nombre del Usuario
  public returnNameUser(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].firstname + " " + this.users[i].lastname;
      }
    }
  }

  //Devolver el Nombre del Usuario
   public returnPicture(idUser:any):any {
    for(var i = 0;i<this.users.length;i++) {
      if(this.users[i].id === idUser) {
        return this.users[i].picture;
      }
    }
  }

  //MENSAJES
  public message(messages: any) {
    this.toast.create({
      message: messages,
      duration: 750
    }).present();
  }

  //CARGAR PAGINA
  openBrowser(urlObject:any) {
    this.iab.create(urlObject, '_blank', 'presentationstyle=pagesheet,toolbarcolor=#E67E22');
  }

  //COPIAR AL PORTAPAPELES
  copyClipboard(parameter:any) {
    this.clipboard.copy(parameter);
    this.message('Zoom ID copiado al portapapeles.')
  }
}
