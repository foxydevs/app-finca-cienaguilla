import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipeModule } from '../../../pipes/pipes.module';
import { TipsBibliaClientPage } from './tips-biblia-client';
 
@NgModule({
  declarations: [
    TipsBibliaClientPage,
  ],
  imports: [
    IonicPageModule.forChild(TipsBibliaClientPage),
    PipeModule
  ],
  exports: [
    TipsBibliaClientPage
  ]
})
export class TipsBibliaClientPageModule {}